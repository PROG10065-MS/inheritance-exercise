﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PALSessionInheritanceVis
{
    // LEAVE ALL CODE AS IS, EXCEPT AS SPECIFIED
    public class MasterOfTheHouse
    {
        private string _name = "Marco";

        private int _learnedClasses = 87;

        public MasterOfTheHouse(int ver = 0)
        {
            if (ver == 0)
            {
                Console.WriteLine($"Hey buddy, I'm the master class. My name is {_name}.");
                Console.WriteLine($"I have mastered the art of {_learnedClasses} classes.");

                Console.WriteLine("It's time for the function to run.");

                TheFunction();

                Console.WriteLine("Now... Let's run through some colors.");

                RunThroughColors();
            }
        }

        // TODO: Add properties for the name and number of learned classes. 
        // Make the learned classes property Read only

        // TODO: Make this function overridable!
        public void TheFunction()
        {
            Console.WriteLine("I am the true master class.");
        }

        // Don't do anything here. This is the master's function... Masterfully written.
        // Simply OBSERVE--
        public void RunThroughColors()
        {
            string[] colorArray = new string[] {"Red", "Orange", "Blue", "Silver"};

            foreach(string color in colorArray)
            {
                Console.WriteLine(color);
            }
        }
    }
}
